import Vue from 'nativescript-vue';

import Dashboard from './components/Dashboard';
import { TNSFontIcon, fonticon } from './nativescript-fonticon';
// Uncommment the following to see NativeScript-Vue output logs
// Vue.config.silent = false;

TNSFontIcon.debug = false;
TNSFontIcon.paths = {
    'fa': './fonts/font-awesome.css',
    'ion': './fonts/ionicons.css',
};
TNSFontIcon.loadCss();
Vue.filter('fonticon', fonticon);

Vue.registerElement(
  'CardView',
  () => require('@nstudio/nativescript-cardview').CardView
);
new Vue({
  render: h => h('frame', [h(Dashboard)])
}).$start();
