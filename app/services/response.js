import {
    alertBox
  } from "./misc";
  
  export function isResponseValid(response, app) {
    console.log(response);
    if (typeof response.data != "object") {
      return false;
    }
  
    if (response.data.status == false) {
      return false;
    } else {
      return true;
    }
  }
  
  export function isErrorHandled(error, app) {
    console.log(error, error.response.status);
    alertBox(error.message);
    var Option = {};
    switch (error.response.status) {
      case 500:
        return true;
      case 429:
        return true;
      case 405:
        return true;
      case 401:
        return true;
      default:
        return false;
    }
  }
  
  export function getPaginationTemplate() {
    return {
      current_page: 1,
      data: [],
      first_page_url: "",
      from: null,
      last_page: 1,
      last_page_url: "",
      next_page_url: null,
      path: "",
      per_page: "",
      prev_page_url: null,
      to: null,
      total: 0
    };
  }