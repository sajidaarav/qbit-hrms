export function alertBox(message) {

  return alert({
    title: "Qbit",
    okButtonText: "OK",
    message: message
  });
}