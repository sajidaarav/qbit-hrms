import Login from "../components/Login";
import Dashboard from "../components/Dashboard";

const routes = {
    login: Login,
    home: Dashboard
}
export default routes;